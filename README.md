# Real Time Twitter Sentiment Analysis (with language conversion)

This uses my sentiment and translation micro-services to analyse streaming tweets for sentiment. 

The Node server checks incoming tweets matching a given term and then converts the tweet to English before checking sentiment. The data is then written to a Mongo database. React and Sockets is then used to pass the data to the front end. 

Demo here: https://tweet-trackr.herokuapp.com

![Screen Shot 2017-07-14 at 15.41.29.png](https://bitbucket.org/repo/jqnBo8/images/3453665290-Screen%20Shot%202017-07-14%20at%2015.41.29.png)

## Requirements

- node / npm / mongo

## Installation

1. Install mongo
2. Edit config example and add your own twitter credentials - save as config.js

## Running

1. Start mongo server (mongod)
2. npm run build
3. node server.js
4. enter your tracking term and watch the console
5. go to localhost:8080 for web view