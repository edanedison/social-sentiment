/** @jsx React.DOM */

var React = require('react');
var createReactClass = require('create-react-class');

module.exports = Tweet = createReactClass({
  render: function(){
    var tweet = this.props.tweet;
    return (
      <li className={"tweet" + (tweet.active ? ' active' : '')}>
        <img src={tweet.avatar} className="avatar"/>
        <blockquote>
          <cite>
            <a href={"http://www.twitter.com/" + tweet.screenname}>{tweet.author}</a> 
            <span className="screen-name">@{tweet.screenname}</span> 
          </cite>
          <span className="content">{tweet.body}</span>          
          <span className="content">{tweet.translation}</span>
        </blockquote>
      </li>
    )
  }
});