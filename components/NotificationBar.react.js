/** @jsx React.DOM */

var React = require('react');
var createReactClass = require('create-react-class');

module.exports = NotificationBar = createReactClass({
  render: function(){
    var count = this.props.count;
    return (
      <div className={"notification-bar" + (count > 0 ? ' active' : '')}>
        <p style={{'textAlign':'center'}}>There are {count} new tweets!</p>
      </div>
    )
  }
});