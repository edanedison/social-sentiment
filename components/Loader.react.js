/** @jsx React.DOM */

var React = require('react');
var createReactClass = require('create-react-class');

module.exports = Loader = createReactClass({
  render: function(){
    return (
      <div className={"loader " + (this.props.paging ? "active" : "")}>
        <img src="svg/loader.svg" />
      </div>
    )
  }
});