var Tweet = require('../models/Tweet');
var emoji = require('node-emoji');
var axios = require('axios');

module.exports = function(stream, io, term) {

    var tweetCleaned = false;
    var tweetTranslation = false;
    var tweetCount = 0;
    var translated;
    var sentiment;

    function saveTweet(data, tweetTranslation, translated, tweetCleaned, sentiment) {

        tweetCount++;

        var tweet = {
            twid: data['id_str'],
            active: false,
            author: data['user']['name'],
            avatar: data['user']['profile_image_url'],
            body: tweetCleaned,
            translation: tweetTranslation,
            date: data['created_at'],
            screenname: data['user']['screen_name']
        };

        var tweetEntry = new Tweet(tweet);
        var emotion = 'neutral_face';

        if (sentiment > 0) {
            emotion = 'heart_eyes';
        } else if (sentiment < 0) {
            emotion = 'rage';
        };

        console.log(tweetCount);
        console.log("Original: " + (data.truncated ? data.extended_tweet.full_text : data.text));
        console.log("Cleaned: " + tweetCleaned);

        if (translated) {
            console.log("Original language: " + data.lang);
            console.log("Translated: " + tweetTranslation);
        }

        console.log("Sentiment: " + sentiment);
        console.log(emoji.get(emotion) + "  ==================================================\n")

        tweetEntry.save(function(err) {
            if (!err) {
                io.emit('tweet', tweet);
                translated = false;
            }
        });
    }

    function getSentiment(data, translated, tweetTranslation, tweetCleaned) {



        axios.get('http://sentiment-microservice.herokuapp.com', {
                params: {
                    text: (translated ? tweetTranslation : tweetCleaned),
                    type: "analyzeSentiment"
                }
            })
            .then(function(response) {
                // console.log(JSON.stringify(response.data, null, 4));
                sentiment = JSON.parse(response.data.documentSentiment.score);
                saveTweet(data, tweetTranslation, translated, tweetCleaned, sentiment);

            })

        .catch(function(error) {
            console.log(error);
        });
    }


    function getTranslation(data, tweetCleaned) {

    var translated = false;
    var tweetTranslation;

    if (data.lang !== 'en') {

        axios.get('http://translate-microservice.herokuapp.com', {
                params: {
                    text: tweetCleaned
                }
            })
            .then(function(response) {
                // console.log(JSON.stringify(response.data, null, 4));
                tweetTranslation = response.data.text;
                translated = true;

                getSentiment(data, translated, tweetTranslation, tweetCleaned)

            })
            .catch(function(error) {
                console.log(error);
            });

    } else {
        getSentiment(data, translated, tweetTranslation, tweetCleaned)
    }



    }

    function cleanUpTweet(data, callback) {

        var phoneRegex = require('phone-regex')
        var emailRegex = require('email-regex')
        var addressRegex = /\b(\d{2,5}\s+)(?![a|p]m\b)(NW|NE|SW|SE|north|south|west|east|n|e|s|w)?([\s|\,|.]+)?(([a-zA-Z|\s+]{1,30}){1,4})(court|ct|street|st|drive|dr|lane|ln|road|rd|blvd)/i // http://stackoverflow.com/questions/9397485/regex-street-address-match thx yo!

        var m, text = (data.truncated ? data.extended_tweet.full_text : data.text);

        text = text.replace(/(?:https?|ftp):\/\/[\n\S]+/g, '');

        while (m = (text.match(phoneRegex()) || text.match(emailRegex()) || text.match(addressRegex))) {
            text = text.replace(m[0], ' ')
        }

        tweetCleaned = text.replace(/@/g, '').replace(/#/g, '').replace(/\s+/g, ' ').replace(/^\s|\s$/g, '');

        callback(tweetCleaned);

    }

    stream.on('data', function(data) {

        if (data['user'] !== undefined && !data.retweeted_status && data.user.followers_count >= 0) {

            cleanUpTweet(data, function(tweetCleaned) {
                getTranslation(data, tweetCleaned);
            });

        }

    });

};