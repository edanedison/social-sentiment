// Require our dependencies
const express = require('express'),
  exphbs = require('express-handlebars'),
  http = require('http'),
  mongoose = require('mongoose'),
  bluebird = require('bluebird'),
  twitter = require('twitter'),
  routes = require('./routes'),
  config = require('./config'),
  streamHandler = require('./utils/streamHandler'),
  readline = require('readline');

// Create an express instance and set a port variable
var app = express();
var port = process.env.PORT || 8080;
var db = process.env.MONGODB_URI || 'mongodb://localhost/tweet-tracker';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Connect to our mongo database
mongoose.Promise = Promise;
mongoose.connect(db);

// Set handlebars as the templating engine
app.engine('handlebars', exphbs({ defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// Disable etag headers on responses
app.disable('etag');

// Create a new ntwitter instance
const twit = new twitter(config.twitter);

// Routes
app.get('/', routes.index);
app.get('/page/:page/:skip', routes.page);
app.use("/", express.static(__dirname + "/public/"));

// Ask a question via node terminal

//rl.question('Enter a tracking term (can be string, phrase, hashtag, or mention)\n', (term) => {

  const term = "Trump";

  console.log('Checking sentiment for the term(s): ', term);
    
  // Start the server
  const server = http.createServer(app).listen(port, function() {
    console.log('Express server listening on port ' + port + '\n');
  });

  // Initialize socket.io
  const io = require('socket.io').listen(server);

  // Set a stream listener for tweets within a given location
  // twit.stream('statuses/filter',{ locations: '-9.74,49.95,1.89,59.76'}, function(stream){
  //   streamHandler(stream,io,term);
  // });

  // twit.stream('statuses/filter',{ track: '@ClairesEurope, @claires, @clairesstores'}, function(stream){
  //   streamHandler(stream,io,term);
  // });  

  twit.stream('statuses/filter',{ track: term}, function(stream){
    streamHandler(stream,io);
  });  
  
// rl.close();

// });